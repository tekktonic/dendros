# Dendros: An [Arbor](https://github.com/arborchat) inspired chat protocol with a focus on decentralization.

## Purpose
Mostly to experiment with something similar to but not-quite Arbor. The idea being that approaching the problem in a couple different ways ought to yield interesting results which would benefit both. While Arbor is more focused on just a proof of concept for tree-based chat, I've taken it a bit further and assumed that tree based chat works, now I'm exploring the benefits that it might provide. I've written it in a literate style mostly for funsies.

## Building
Dendros is written in org-babel in a literate style. You need emacs to "tangle" the source code from the accompanying document. Thankfully the makefile I'll write eventually should take care of this process for you, assuming you have emacs installed.
